package tests

import (
	"github.com/revel/revel/testing"
)

type AppTest struct {
	testing.TestSuite
}

func (t *AppTest) Before() {
	println("Set up")
}

func (t *AppTest) TestIndexPage() {
	t.Get("/")
	t.AssertOk()
	t.AssertContentType("text/html; charset=utf-8")
}

// Will not appear in panel as it does not start with `Test`.
func (t *AppTest) TestFavIcon() {
	t.Get("/public/img/favicon.png")
	t.AssertOk()
	t.AssertContentType("image/png")
}

func (t *AppTest) TestItemList() {
	t.Get("/items")
	t.AssertOk()
	t.AssertContentType("application/json; charset=utf-8")
}

func (t *AppTest) After() {
	println("Tear down")
}
