function load(page,div){    
    $.ajax({
        // url: site+page,
        url: 'http://localhost:8000'+page,
        beforeSend: function(){
            $(div).html('<div id="LoadingBlock">'+image_load + '</div>');
        },
        success: function(response){
			$(div).css("display", "none");		
			$(div).fadeIn(300);			
            $(div).html(response);              
        },
        dataType:"html"  		
    });    
    return false;
}
function loadPage(page,div,menu,submenu){    
    $.ajax({
        // url: site+page,
        url: 'http://localhost:8000'+page,
        beforeSend: function(){
            $(div).html('<div id="LoadingBlock">'+image_load + '</div>');
        },
        success: function(response){
            $("li[id|='menu']" ).removeClass("active");  // this deactivates the home tab
            $("#menu-"+menu).addClass("active");
            ChangeHeaderBreadcumb(menu,submenu);
            $(div).css("display", "none");      
            $(div).fadeIn(300);         
            $(div).html(response);              
        },
        dataType:"html"         
    });    
    return false;
}
function ChangeHeaderBreadcumb(menu,submenu){
    if (submenu == ""){
        $('.content-header').html('<div><ol class="breadcrumb"><li><a href="#">Home</a></li><li class="active">'+menu+'</li></ol></div>');
    } else {
        $('.content-header').html('<div><ol class="breadcrumb"><li><a href="#">Home</a></li><li>'+menu+'</li><li class="active">'+submenu+'</li></ol></div>');
    }
    return false;
}
function load_no_loading(page,div){    
    $.ajax({
        url: site+page,
        beforeSend: function(){
            $(div).html('<div id="LoadingBlock">'+image_load + '</div>');       
        },
        success: function(response){
            $(div).css("display", "none");      
            $(div).fadeIn(300);         
            $(div).html(response);              
        },
        dataType:"html"         
    });    
    return false;
}
function AjaxSending(page,div,actionurl){    

    $.post(actionurl, function( data ) {
        $.get(site+page, function( data ) {
          $(div).html( data );  
        });
    });

    return false;
}
function DeptChanges(actionurl,div){    

    var jqxhr = $.post( actionurl, function() {
      location.reload();      
    });

    return true;
}
function AjaxSendWithSearch(page,div,actionurl){

    $.post(actionurl, function( data ) {

        $.ajax({
            url: site+page, 
            data: $(document.search_form.elements).serialize(),
            error: function (request, error) {            
                alert(" What can I do, an error happened : " + error);
            },
            beforeSend: function(){
                $(div).html('<div id="LoadingBlock">'+image_load + '</div>');
            },
            success: function(response){
                    if(!response)
                    {
                        alert('error');
                        //load_into_box("app/error_confirmation");
                    }
                    else
                    {
                        $(div).css("display", "none");      
                        $(div).fadeIn(300);
                        $(div).html(response);                    
                    }
                },
            type: "POST",
            dataType: "html"
        });     

    });    

    return false;
}
function send_form(formObj,action,responseDIV)
{
    $.ajax({
        url: site+action, 
        data: $(formObj.elements).serialize(),
        error: function (request, error) {            
            alert(" What can I do, an error happened : " + error);
        },
        beforeSend: function(){
            $(responseDIV).html('<div id="LoadingBlock">'+image_load + '</div>');
        },
        success: function(response){
                if(!response)
                {
                    alert('error');
                    //load_into_box("app/error_confirmation");
                }
                else
                {
                    $(responseDIV).css("display", "none");      
                    $(responseDIV).fadeIn(300);
                    $(responseDIV).html(response);                    
                }
            },
        type: "POST",
        dataType: "html"
    });     

    return false;
}
function send_form_refresh(formObj,action,responseDIV, load_url)
{    
    $.ajax({
        url: site+action, 
        data: $(formObj.elements).serialize(),
        error: function (request, error) {            
            alert(" What can I do, an error happened : " + error);
        },
        success: function(response){
                if(!response)
                {
                    alert('error');
                    //load_into_box("app/error_confirmation");
                }
                else
                {
                    load(load_url, responseDIV);                  
                }
            },
        type: "post", 
        dataType: "html"
    });     
    return false;
}
function Searching(action,div)
{
    $.ajax({
        url: site+action, 
        data: $(document.search_form.elements).serialize(),
        error: function (request, error) {            
            alert(" What can I do, an error happened : " + error);
        },
        beforeSend: function(){
            $(div).html('<div id="LoadingBlock">'+image_load + '</div>');
        },
        success: function(response){
                if(!response)
                {
                    alert('error');
                    //load_into_box("app/error_confirmation");
                }
                else
                {
                    $(div).css("display", "none");      
                    $(div).fadeIn(300);
                    $(div).html(response);                    
                }
            },
        type: "POST",
        dataType: "html"
    });     

    return false;
}
function SearchPagingLoad(a,b,c){
    filterValue = $('#filterValue').val();
    filterField = $('#filterField').val();
    // filterValue = filterValue.replace(/%/g,'');    
    load(a+b+'/'+filterField+'/'+filterValue,c);
}
function SearchLoad(a,c){
    filterValue = $('#filterValue').val();
    filterField = $('#filterField').val();
    // filterValue = filterValue.replace(/%/g,'');    
    load(a+'/'+filterField+'/'+filterValue,c);
}
function LoadMorePost(page, div){
  var desc = $(div).html();
  if(desc == '' || desc == null){
    load(page, div);
  }else{
    return true;
  }
}
function ActivateTab(id){    
    $('#NavigateTab a[href="#'+id+'"]').tab('show');    
}