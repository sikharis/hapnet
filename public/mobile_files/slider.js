window.onload = function () {

	initSlider();
	function initSlider() {

		var slide = document.getElementsByClassName("slide_item");

		var cont = document.getElementsByClassName("slider")[0];
		var composed = getComputedStyle(slide[1]);

		var pos = 0;
		setInterval(function () {
			cont.style.height = composed.height;
			if ( pos ==3 )
			{
				pos = 0;
			}

			if ( pos == 0 )
			{
				slide[0].style.left = "0";
				slide[1].style.left = "100%";
				slide[2].style.left = "200%";

			}
			if ( pos == 1 )
			{
				slide[0].style.left = "-100%";
				slide[1].style.left = "0";
				slide[2].style.left = "100%";

			}
			if ( pos == 2 )
			{
				slide[0].style.left = "-200%";
				slide[1].style.left = "-100%";
				slide[2].style.left = "0";

			}

			pos++;

		}, 6000);
	}
};

