var slider;
$(document).ready(function(){
		$('.jsOpenWindow').on('touchend, click', function(e){
			var ZZ=$('.order_form');
			if(ZZ.length>0){
				$('html, body').animate({scrollTop: ZZ.offset().top}, 800);
			}else{
				e.preventDefault();
				$('.dbody').remove();
				$('html, body').animate({scrollTop: 0}, 300);
				$('.hidden-window').fadeIn();
				slider = $('.bx-bx').bxSlider({                            
					pagerCustom: '#bx-pager',                       
					adaptiveHeight: true,
					touchEnabled: true,                                                             
					onSlideBefore : function($slideElement, oldIndex, newIndex){
						var ValOptionChg = $slideElement.attr('data-value');
						if (typeof set_package_prices == 'function') {set_package_prices(ValOptionChg);}
						$("select[name='count_select'] option[value='"+ValOptionChg+"']").prop("selected", "selected");
					}
				});
				slider.goToSlide(1);
				return false;
			}
		});
		if(typeof set_package_prices == 'function'){
			set_package_prices(3);
		}
		$('.corbselect, .change-package-selector').on('change', function(){
			slider.goToSlide($(this).find('option:selected').attr('data-slide-index'));
		});

});
