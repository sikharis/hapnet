FROM alpine:edge
MAINTAINER Kharis Hidayatullah <kharis.hidayatullah@gmail.com>
ADD main /main
RUN chmod 700 /main
CMD "/main"