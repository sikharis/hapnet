package controllers

import (
	// "fmt"
	"github.com/revel/revel"
	"hapnet/app/models"
)

func (c App) PageOrder() revel.Result {

	pageVal := c.getSessionOrParamInt("page", 1)
	limit := c.getSessionOrParamInt("limit", 5)
	search := c.getSessionOrParamStr("search", "")
	clearSearch := c.getSessionOrParamInt("clearSearch", 0)

	if clearSearch == 1 {
		c.Session["search"] = ""
		c.Session["clearSearch"] = "0"
		search = ""
	}

	revel.INFO.Println("##", search)
	page := pageVal - 1
	var orders = []models.Order{}
	var TotalCount int
	currentBtn := page
	offset := page * limit

	if search == "" {
		Db.Find(&orders).Count(&TotalCount)
		Db.Limit(limit).Offset(offset).Find(&orders)
	} else {
		Db.Where("CONCAT(name, phone) like ?", "%"+search+"%").Find(&orders).Count(&TotalCount)
		Db.Where("CONCAT(name, phone) like ?", "%"+search+"%").Limit(limit).Offset(offset).Find(&orders)
	}

	numberOfBtns := getNumberOfButtonsForPagination(TotalCount, limit)
	sliceBtns := createSliceForBtns(numberOfBtns)
	startIndex := offset + 1

	return c.Render(orders, sliceBtns, currentBtn, startIndex)
}

func (c App) OrderList() revel.Result {
	data := make(map[string]interface{})
	order := []models.Order{}
	data["success"] = true
	data["payload"] = nil
	// stuff := Stuff{Foo: "xyz", Bar: 999}
	if err := Db.Find(&order).Error; err != nil {
		data["success"] = false
		data["error"] = Error{Code: 500, Message: "Internal error"}
		return c.RenderJSON(data)
	}
	data["payload"] = order
	return c.RenderJSON(data)
}

func (c App) PageOrderEdit(groupby string, value string) revel.Result {
	orders := []models.Order{}
	Db.Where(groupby+"=?", value).Find(&orders)

	return c.Render(orders)
}
