package controllers

import (
	"github.com/leekchan/accounting"
	"github.com/revel/revel"
	// "gopkg.in/eapache/queue.v1"
	"strconv"
	"time"
)

var Ac *accounting.Accounting

func init() {
	// q := queue.New()

	// revel.TemplateFuncs["register_template"] = func(template_name string) string {
	// 	q.Add(template_name)
	// 	// fmt.Printf("%s", template_name)
	// 	return ""
	// }

	// revel.TemplateFuncs["template_order"] = func() string {
	// 	// s := q.Get(0).(string)
	// 	q.Remove()
	// 	// template.ParseFiles(q.Get(0).(string))
	// 	// template.Template.Execute(revel.CompressResponseWriter, q.Get(0).(string)	)
	// 	return q.Get(0).(string)
	// }
	revel.TemplateFuncs["seqno"] = func(x, y int) int {
		return x + y
	}
	revel.TemplateFuncs["timeFormat"] = func(val time.Time) string {
		return val.UTC().Add(7 * time.Hour).Format("02-01-2006 15:04:05")
	}
	revel.TemplateFuncs["currency"] = func(value string) string {
		// column, _ := decimal.NewFromString(v)
		val, err := strconv.ParseInt(value, 10, 64)
		if err != nil {
			revel.INFO.Println(err.Error())
		}
		return Ac.FormatMoney(val)
	}

	revel.OnAppStart(InitDB)
	revel.OnAppStart(InitGlobal)
	// revel.InterceptMethod((*GormController).Begin, revel.BEFORE)
	// revel.InterceptMethod((*GormController).Commit, revel.AFTER)
	// revel.InterceptMethod((*GormController).Rollback, revel.FINALLY)

}

func InitGlobal() {
	Ac = &accounting.Accounting{Symbol: "Rp ", Precision: 0, Thousand: ".", Decimal: ","}
	revel.INFO.Println(Ac.FormatMoney(123456789.213123)) // "$123,456,789.21"
}
