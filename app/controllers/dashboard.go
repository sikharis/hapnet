package controllers

import (
	"github.com/revel/revel"
	"hapnet/app/routes"
)

//Index for Landing pages
func (c App) Dashboard() revel.Result {
	baseURL := c.BaseUrl()
	if !c.isAuthorized() {
		c.Flash.Error("Please log in first")
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(routes.App.Login())
	}

	return c.Render(baseURL)
}

func (c App) PageDashboard() revel.Result {
	return c.Render()
}
