package controllers

import (
	"errors"
	"github.com/revel/revel"
	"hapnet/app/models"
)

// Return a list of all the networks
func getAllNetworks() []models.Network {
	var network = []models.Network{}
	if err := Db.Find(&network).Error; err != nil {
		// fmt.Println(err)
		return network
	}
	return network
}

// Fetch an network based on the ID supplied
func getNetworkByID(id int) (*models.Network, error) {
	var network = models.Network{}
	if err := Db.First(&network, 10).Error; err != nil {
		// fmt.Println(err)
		return &network, err
	}
	return nil, errors.New("Network not found")
}

func (c App) PageNetwork() revel.Result {

	pageVal := c.getSessionOrParamInt("page", 1)
	limit := c.getSessionOrParamInt("limit", 5)
	search := c.getSessionOrParamStr("search", "")
	clearSearch := c.getSessionOrParamInt("clearSearch", 0)

	if clearSearch == 1 {
		c.Session["search"] = ""
		c.Session["clearSearch"] = "0"
		search = ""
	}

	revel.INFO.Println("##", search)
	page := pageVal - 1
	var networks = []models.Network{}
	var TotalCount int
	currentBtn := page
	offset := page * limit

	if search == "" {
		Db.Find(&networks).Count(&TotalCount)
		Db.Limit(limit).Offset(offset).Find(&networks)
	} else {
		Db.Where("network_name like ?", "%"+search+"%").Find(&networks).Count(&TotalCount)
		Db.Where("network_name like ?", "%"+search+"%").Limit(limit).Offset(offset).Find(&networks)
	}

	numberOfBtns := getNumberOfButtonsForPagination(TotalCount, limit)
	sliceBtns := createSliceForBtns(numberOfBtns)
	startIndex := offset + 1

	return c.Render(networks, sliceBtns, currentBtn, startIndex)
}

func (c App) PageNetworkEdit(NetworkID int) revel.Result {
	network := models.Network{}

	if c.Request.Method == "POST" {
		c.Controller.Params.Bind(&network, "network")

		if network.ID != 0 {
			c.Validation.Required(network.ID).Message("Network ID is required.")
			if err := Db.Save(&network).Error; err != nil {
				c.Flash.Error(err.Error())
				c.Validation.Keep()
				c.FlashParams()
				return c.Render(network)
			}

			c.Flash.Success("Save succeed.")
			return c.Render(network)
		} else {
			if err := Db.Create(&network).Error; err != nil {
				c.Flash.Error(err.Error())
				c.Validation.Keep()
				c.FlashParams()
				return c.Render(network)
			}
		}
	}
	revel.INFO.Println("method: ", c.Request.Method, ", NetworkID: ", NetworkID)
	if c.Request.Method == "GET" && NetworkID != 0 {
		Db.Where("ID=?", NetworkID).Find(&network)
		return c.Render(network)
	}

	return c.Render(network)
}
