package controllers

import (
	"github.com/revel/revel"
	"github.com/rs/xid"
	"hapnet/app/models"
	// "hapnet/app/routes"
	// "github.com/jinzhu/gorm"
	"strings"
)

func (c App) Traffic() revel.Result {

	guid := xid.New()
	guidTime := guid.Time()

	networkID := parseIntOrDefault(c.Params.Query.Get("networkID"), 0)
	clickID := parseIntOrDefault(c.Params.Query.Get("clickID"), 0)
	tid := parseIntOrDefault(c.Params.Query.Get("tid"), 0)
	page := parseIntOrDefault(c.Params.Query.Get("page"), 0)
	gclid := parseIntOrDefault(c.Params.Query.Get("gclid"), 0)
	deviceType := string(GetDeviceType(c.Request))

	p1 := c.Params.Query.Get("p1")
	p2 := c.Params.Query.Get("p2")
	p3 := c.Params.Query.Get("p3")
	p4 := c.Params.Query.Get("p4")
	p5 := c.Params.Query.Get("p5")

	// Get client IP, optional
	clientIP := c.Request.Header.Get("X-Real-IP")
	if clientIP == "" {
		clientIP = strings.Split(c.Request.RemoteAddr, ":")[0]
	}

	tls := "http://"
	if c.Request.TLS != nil {
		tls = "https://"
	}

	// fmt.Println(networkID)
	traffic := &models.Traffic{
		GUID:       guid.String(),
		GUIDTime:   guidTime,
		URL:        tls + c.Request.Host + c.Request.URL.RequestURI(),
		IP:         clientIP,
		Page:       page,
		NetworkID:  networkID,
		ClickID:    clickID,
		TID:        tid,
		GCLID:      gclid,
		UserAgent:  c.Request.UserAgent(),
		DeviceType: deviceType,
		P1:         p1,
		P2:         p2,
		P3:         p3,
		P4:         p4,
		P5:         p5,
	}

	// // //Validation
	// c.Validation.Required(guid.String())
	// c.Validation.MinSize(traffic.GUID, 20)
	// c.Validation.MaxSize(traffic.GUID, 20)
	// c.Validation.Required(traffic.NetworkID)
	// c.Validation.Required(traffic.ClickID)
	// c.Validation.Required(traffic.GUIDTime)
	// c.Validation.Required(traffic.URL)

	// if c.Validation.HasErrors() {
	// 	// Store the validation errors in the flash context and redirect.
	// 	c.Flash.Error("User not authenticated.")
	// 	c.Validation.Keep()
	// 	c.FlashParams()
	// 	revel.INFO.Println("validation error: save order")
	// 	return c.Render(traffic)
	// }

	Db.Create(traffic)

	c.ViewArgs["traffic"] = traffic
	return PageProcess(&c.GormController, traffic)
}
