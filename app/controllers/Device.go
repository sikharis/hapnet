package controllers

import (
	"strings"

	"github.com/revel/revel"
)

type DeviceController struct {
	*revel.Controller
}

type DeviceType string

const (
	MOBILE DeviceType = "MOBILE"
	TABLET DeviceType = "TAB"
	WEB    DeviceType = "WEB"
	TV     DeviceType = "TV"
)

func GetDeviceType(r *revel.Request) DeviceType {

	if isUserAgent(r, "Android", "webOS", "iPhone", "BlackBerry", "Windows Phone") {
		return MOBILE
	}
	if isUserAgent(r, "iPad", "iPod", "tablet", "RX-34", "FOLIO") ||
		(isUserAgent(r, "Kindle", "Mac OS") && isUserAgent(r, "Silk")) ||
		(isUserAgent(r, "AppleWebKit") && isUserAgent(r, "Silk")) {
		return TABLET
	}
	if isUserAgent(r, "TV", "NetCast", "boxee", "Kylo", "Roku", "DLNADOC") {
		return TV
	}

	return WEB
}

func isUserAgent(r *revel.Request, userAgents ...string) bool {
	userAgent := r.Header.Get("User-Agent")
	for _, v := range userAgents {
		if strings.Contains(userAgent, v) {
			return true
		}
	}
	return false
}
