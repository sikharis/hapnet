package controllers

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"github.com/revel/revel"
	"golang.org/x/crypto/bcrypt"
	"hapnet/app/models"
	"strings"
	// "time"
)

type GormController struct {
	*revel.Controller
	Tx *gorm.DB
}

var Db *gorm.DB

func InitDB() {
	var err error
	revel.INFO.Println(getConnectionString())
	Db, err = gorm.Open("mysql", getConnectionString())
	if err != nil {
		revel.ERROR.Println("FATAL", err)
		panic(err)
	}

	Db.SingularTable(true)
	// Db.DropTableIfExists(&models.Order{})
	Db.Set("gorm:table_options", "ENGINE=InnoDB").AutoMigrate(
		&models.Provinsi{},
		&models.CityKab{},
		&models.Kecamatan{},
		&models.Kelurahan{},
		&models.Contact{},
		&models.Menu{},
		&models.Network{},
		&models.Order{},
		&models.Traffic{},
		&models.ProductTemplate{},
		&models.Logistic{},
		&models.ProductStats{},
		&models.Page{},
		&models.PageAccess{})
	Db.DB().SetMaxIdleConns(10)
	Db.DB().SetMaxOpenConns(100)
	Db.LogMode(true)

	//INITIALIZE PRODUCT TABLE
	if !Db.HasTable(&models.User{}) {
		Db.Set("gorm:table_options", "ENGINE=InnoDB").AutoMigrate(
			&models.User{})

		bcryptPassword, _ := bcrypt.GenerateFromPassword([]byte("password"), bcrypt.DefaultCost)
		var userdata = &models.User{
			Name:           "Administrator",
			Username:       "Administrator",
			Password:       "password",
			HashedPassword: bcryptPassword,
		}
		Db.Create(userdata)
	}

	//INITIALIZE PRODUCT TABLE
	if !Db.HasTable(&models.Product{}) {
		Db.Set("gorm:table_options", "ENGINE=InnoDB").AutoMigrate(
			&models.Product{})

		var product = &models.Product{
			ProductCode: "HRCLS",
			Name:        "Hercules",
			Description: "Hercules",
			Domain:      "localhost:8000",
			Active:      true,
		}
		Db.Create(product)
	}

	//INITIALIZE ROLE TABLE
	if !Db.HasTable(&models.Role{}) {
		Db.Set("gorm:table_options", "ENGINE=InnoDB").AutoMigrate(
			&models.Role{})

		var SA = &models.Role{Role: "SA", Description: "Super Admin"}
		var CS = &models.Role{Role: "CS", Description: "Customer Service"}
		var LOG = &models.Role{Role: "LOG", Description: "Logistics"}
		var FIN = &models.Role{Role: "FIN", Description: "Finance"}

		Db.Create(SA)
		Db.Create(CS)
		Db.Create(LOG)
		Db.Create(FIN)
	}

	//INITIALIZE STATUS TABLE
	if !Db.HasTable(&models.Status{}) {
		Db.Set("gorm:table_options", "ENGINE=InnoDB").AutoMigrate(
			&models.Status{})

		var Status1 = &models.Status{StatusCode: "NEW_ORDER", Name: "New Order", Description: ""}
		var Status2 = &models.Status{StatusCode: "PENDING", Name: "Pending", Description: ""}
		var Status3 = &models.Status{StatusCode: "CANCEL", Name: "Cancel", Description: ""}
		var Status4 = &models.Status{StatusCode: "SALE", Name: "Sale", Description: ""}
		var Status5 = &models.Status{StatusCode: "CONFIRM_BUY", Name: "Confirm Buy", Description: ""}
		var Status6 = &models.Status{StatusCode: "CANCEL_FOLLOW_UP", Name: "Cancel Follow Up", Description: ""}
		var Status7 = &models.Status{StatusCode: "CANCEL_FINANCE", Name: "Cancel Finance", Description: ""}

		Db.Create(Status1)
		Db.Create(Status2)
		Db.Create(Status3)
		Db.Create(Status4)
		Db.Create(Status5)
		Db.Create(Status6)
		Db.Create(Status7)
	}

	if !Db.HasTable(&models.StatusAccess{}) {

		Db.Set("gorm:table_options", "ENGINE=InnoDB").AutoMigrate(
			&models.StatusAccess{})

		var sa1 = &models.StatusAccess{StatusId: 1, NextStatusId: 5, Order: 1, Active: true}
		var sa2 = &models.StatusAccess{StatusId: 1, NextStatusId: 3, Order: 1, Active: true}

		Db.Create(sa1)
		Db.Create(sa2)
	}
	// product := []models.Product{}
	// if err := Db.Where("Active = ?", 1).Find(&product).Error; err != nil {
	// 	data["success"] = false
	// 	data["error"] = Error{Code: 500, Message: "Internal error"}
	// 	return c.RenderJSON(data)
	// }

	// i := 1
	// for _, v := range product {
	// 	v.
	// }

	// var network = &models.Network{
	// 	NetworkID:     0,
	// 	NetworkName:   "Website",
	// 	PostbackURL:   "",
	// 	PostbackLeads: "",
	// 	PostbackSales: "",
	// 	ClickIDSale:   ""}

	// Db.Create(network)

	// var P1T1 = &models.ProductTemplate{ProductId: 1, NextId: 2, Name: "Hercules Page 1", URL: "Template/T1_P1.html", Active: true}
	// var P2T2 = &models.ProductTemplate{ProductId: 1, NextId: 3, Name: "Hercules Page 2", URL: "Template/T1_P2.html", Active: true}
	// var P3T3 = &models.ProductTemplate{ProductId: 1, NextId: 0, Name: "Hercules Page 3 Thank you", URL: "Template/T1_P3.html", Active: true}

	// Db.Create(P1T1)
	// Db.Create(P2T2)
	// Db.Create(P3T3)
	// var contact = &[]models.Contact{
	// 	{Description: "Telephone"},
	// 	{Description: "SMS"},
	// 	{Description: "Whatsapp"},
	// }

	// Db.Create(contact)

	// min := 100
	// for i := 1; i <= 100; i++ {

	// 	// guid := xid.New()

	// 	table := models.Order{
	// 		OrderID:       fmt.Sprintf("phone-%d", i),
	// 		Phone:         fmt.Sprintf("phone-%d", i),
	// 		ProductCode:   fmt.Sprintf("user-%d", i),
	// 		Status:        "test",
	// 		Name:          fmt.Sprintf("order-%d", i),
	// 		Address:       fmt.Sprintf("order-%d", i),
	// 		NumberOfOrder: i,
	// 		PacketType:    fmt.Sprintf("order-%d", i),
	// 		Email:         fmt.Sprintf("order-%d", i),
	// 		//Payment
	// 		PaymentMethod: fmt.Sprintf("order-%d", i),
	// 		BankCode:      fmt.Sprintf("order-%d", i),
	// 		PaymentAmount: fmt.Sprintf("order-%d", i),
	// 		//Address
	// 		ProvinsiID:  fmt.Sprintf("order-%d", i),
	// 		CityKabID:   fmt.Sprintf("order-%d", i),
	// 		KecamatanID: fmt.Sprintf("order-%d", i),
	// 		KelurahanID: fmt.Sprintf("order-%d", i),
	// 	}

	// 	Db.Create(&table)
	// 	min = min - 1
	// }

}

func getParamString(param string, defaultValue string) string {
	p, found := revel.Config.String(param)
	if !found {
		if defaultValue == "" {
			revel.ERROR.Fatal("Cound not find parameter: " + param)
		} else {
			return defaultValue
		}
	}
	return p
}

func getConnectionString() string {
	host := getParamString("db.host", "localhost")
	port := getParamString("db.port", "3306")
	user := getParamString("db.user", "root")
	pass := getParamString("db.password", "")
	dbname := getParamString("db.name", "hapdb")
	protocol := getParamString("db.protocol", "tcp")
	dbargs := getParamString("dbargs", "parseTime=true")

	if strings.Trim(dbargs, " ") != "" {
		dbargs = "?" + dbargs
	} else {
		dbargs = ""
	}
	return fmt.Sprintf("%s:%s@%s([%s]:%s)/%s%s",
		user, pass, protocol, host, port, dbname, dbargs)
}

func (c *GormController) Begin() revel.Result {
	txn := Db.Begin()
	if txn.Error != nil {
		panic(txn.Error)
	}
	c.Tx = txn
	// revel.INFO.Println("c.Tx init", c.Tx)
	return nil
}
func (c *GormController) Commit() revel.Result {
	if c.Tx == nil {
		return nil
	}
	c.Tx.Commit()
	if err := c.Tx.Error; err != nil && err != sql.ErrTxDone {
		panic(err)
	}
	c.Tx = nil
	revel.INFO.Println("c.Tx commited (nil)")
	return nil
}

func (c *GormController) Rollback() revel.Result {
	if c.Tx == nil {
		return nil
	}
	c.Tx.Rollback()
	if err := c.Tx.Error; err != nil && err != sql.ErrTxDone {
		panic(err)
	}
	c.Tx = nil
	return nil
}
