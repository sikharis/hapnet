package controllers

import (
	"github.com/revel/revel"
	"strconv"
)

func addHeaderCORS(c *revel.Controller) revel.Result {
	c.Response.Out.Header().Add("Access-Control-Allow-Origin", "*")
	return nil
}

func parseUintOrDefault(intStr string, _default uint64) uint64 {
	if value, err := strconv.ParseUint(intStr, 0, 64); err != nil {
		return _default
	} else {
		return value
	}
}

func parseInt64OrDefault(intStr string, _default int64) int64 {
	if value, err := strconv.ParseInt(intStr, 0, 64); err != nil {
		return _default
	} else {
		return value
	}
}

func parseIntOrDefault(intStr string, _default int) int {
	if value, err := strconv.Atoi(intStr); err != nil {
		return _default
	} else {
		return value
	}
}

func setSession(intStr string, _default int) int {
	if value, err := strconv.Atoi(intStr); err != nil {
		return _default
	} else {
		return value
	}
}

func getNumberOfButtonsForPagination(TotalCount int, limit int) int {
	num := (int)(TotalCount / limit)
	if TotalCount%limit > 0 {
		num++
	}
	return num
}

func createSliceForBtns(number int) []int {
	var sliceOfBtn []int
	for i := 0; i < number; i++ {
		sliceOfBtn = append(sliceOfBtn, i+1)
	}
	return sliceOfBtn
}

func (c App) getSessionOrParamInt(key string, _default int) int {
	if c.Params.Query.Get(key) != "" {
		c.Session[key] = c.Params.Query.Get(key)
		return parseIntOrDefault(c.Session[key], _default)
	} else {
		return parseIntOrDefault(c.Session[key], _default)
	}
}
func (c App) getSessionOrParamStr(key string, _default string) string {
	if c.Params.Query.Get(key) != "" {
		c.Session[key] = c.Params.Query.Get(key)
		return c.Session[key]
	}

	if c.Session[key] != "" {
		return c.Session[key]
	} else {
		return _default
	}
}
