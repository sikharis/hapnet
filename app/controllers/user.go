package controllers

import (
	"github.com/revel/revel"
	"golang.org/x/crypto/bcrypt"
	"hapnet/app/models"
)

func (c App) PageUsers() revel.Result {

	pageVal := c.getSessionOrParamInt("page", 1)
	limit := c.getSessionOrParamInt("limit", 5)
	search := c.getSessionOrParamStr("search", "")
	clearSearch := c.getSessionOrParamInt("clearSearch", 0)

	if clearSearch == 1 {
		c.Session["search"] = ""
		c.Session["clearSearch"] = "0"
		search = ""
	}

	revel.INFO.Println("##", search)
	page := pageVal - 1
	var users = []models.User{}
	var TotalCount int
	currentBtn := page
	offset := page * limit

	if search == "" {
		Db.Find(&users).Count(&TotalCount)
		Db.Limit(limit).Offset(offset).Find(&users)
	} else {
		Db.Where("user_name like ?", "%"+search+"%").Find(&users).Count(&TotalCount)
		Db.Where("user_name like ?", "%"+search+"%").Limit(limit).Offset(offset).Find(&users)
	}

	numberOfBtns := getNumberOfButtonsForPagination(TotalCount, limit)
	sliceBtns := createSliceForBtns(numberOfBtns)
	startIndex := offset + 1

	return c.Render(users, sliceBtns, currentBtn, startIndex)
}

func (c App) UserList() revel.Result {
	data := make(map[string]interface{})
	user := []models.User{}
	data["success"] = true
	data["payload"] = nil
	// stuff := Stuff{Foo: "xyz", Bar: 999}
	if err := Db.Find(&user).Error; err != nil {
		data["success"] = false
		data["error"] = Error{Code: 500, Message: "Internal error"}
		return c.RenderJSON(data)
	}
	data["payload"] = user
	return c.RenderJSON(data)
}

func (c App) PageUserEdit(UserID int) revel.Result {
	user := models.User{}

	if c.Request.Method == "POST" {
		c.Controller.Params.Bind(&user, "user")

		if user.ID != 0 {
			bcryptPassword, _ := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
			user.HashedPassword = bcryptPassword
			if err := Db.Save(&user).Error; err != nil {
				c.Flash.Error(err.Error())
				c.Validation.Keep()
				c.FlashParams()
				return c.Render(user)
			}
		} else {
			if err := Db.Create(&user).Error; err != nil {
				c.Flash.Error(err.Error())
				c.Validation.Keep()
				c.FlashParams()
				return c.Render(user)
			}
		}
	}
	revel.INFO.Println("method: ", c.Request.Method, ", UserID: ", UserID)
	if c.Request.Method == "GET" && UserID != 0 {
		Db.Where("ID=?", UserID).Find(&user)
		return c.Render(user)
	}

	return c.Render(user)
}
