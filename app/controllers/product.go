package controllers

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/revel/revel"
	"hapnet/app/models"
	"net"
	"net/http"
	"time"
)

type Product struct {
	GormController
}

// Host returns the address and port of the server, e.g. "127.0.0.1:8557"
func baseHost() string {
	return revel.Config.StringDefault("http.addr", "haha")
}

func (c Product) Page(traffic models.Traffic) revel.Result {
	return c.Render(traffic)
}

func (c Product) Switcher() revel.Result {
	var traffic models.Traffic
	c.Params.Bind(&traffic, "traffic")
	fmt.Println("switcher " + fmt.Sprintf("%s", traffic.Page) + " : " + traffic.GUID)

	return c.Render(traffic)
}

func (c Product) NextPage() revel.Result {
	var traffic models.Traffic
	var order models.Order

	c.Controller.Params.Bind(&traffic, "traffic")
	c.Controller.Params.Bind(&order, "order")

	if order.OrderID != "" {
		OrderProcess(&c.GormController, &traffic, &order)
	}

	return PageProcess(&c.GormController, &traffic)
}

func PageProcess(c *GormController, traffic *models.Traffic) revel.Result {

	data := make(map[string]interface{})
	data["success"] = true
	data["payload"] = nil

	prodTemplate := new(models.ProductTemplate)
	product := new(models.Product)
	productStat := new(models.ProductStats)

	if err := Db.Where("domain=?", baseHost()).First(&product).Error; err != nil {
		data["success"] = false
		data["error"] = Error{Code: 500, Message: "Post back error."}
		data["payload"] = traffic
		return c.RenderJSON(data)
	}
	revel.INFO.Println(traffic.Page)
	// if traffic.Page != 0 {
	// 	traffic.Page = traffic.Page + 1
	// } else
	var page int
	if traffic.Page != 0 {
		page = traffic.Page
	} else {
		if err := Db.Where("product_id=? and landing_page=?", product.ID, 1).Order("count asc").First(&productStat).Error; err != nil {
			data["success"] = false
			data["error"] = Error{Code: 500, Message: err.Error()}
			data["payload"] = traffic
			return c.RenderJSON(data)
		}

		page = productStat.TemplateId
	}

	if err := Db.Where("product_id=? and id = ?", product.ID, page).First(&prodTemplate).Error; err != nil {
		data["success"] = false
		data["error"] = Error{Code: 500, Message: "PT error."}
		data["payload"] = traffic
		return c.RenderJSON(data)
	}

	traffic.Page = prodTemplate.NextId

	Db.Model(models.ProductStats{}).Where("product_id = ? and template_id = ?", prodTemplate.ProductId, prodTemplate.ID).Update("count", gorm.Expr("count + 1"))

	c.ViewArgs["traffic"] = traffic
	return c.RenderTemplate(prodTemplate.URL)
}

func (c Product) Order() revel.Result {

	var order models.Order
	c.Params.Bind(&order, "order")

	var traffic models.Traffic
	c.Params.Bind(&traffic, "traffic")

	return OrderProcess(&c.GormController, &traffic, &order)
}

func OrderProcess(c *GormController, traffic *models.Traffic, order *models.Order) revel.Result {
	data := make(map[string]interface{})
	data["success"] = true
	data["payload"] = nil

	// OrderProcess(&traffic, &order, &data)
	//Save order data
	Db.Create(order)
	//Get Network data for post back
	network := new(models.Network)
	if err := Db.Where("Network_ID=?", traffic.NetworkID).First(network).Error; err != nil {
		data["success"] = false
		data["error"] = Error{Code: 500, Message: "Post back error."}
		data["payload"] = order
		return c.RenderJSON(data)
	}

	//Post back section
	var netTransport = &http.Transport{
		Dial: (&net.Dialer{
			Timeout: 5 * time.Second,
		}).Dial,
		TLSHandshakeTimeout: 5 * time.Second,
	}
	var netClient = &http.Client{
		Timeout:   time.Second * 10,
		Transport: netTransport,
	}
	// fmt.Println(network.PostbackLeads)
	netClient.Get(network.PostbackLeads)

	//Render payload and JSON
	data["payload"] = order
	return c.RenderJSON(data)
}
