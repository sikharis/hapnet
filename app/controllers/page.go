package controllers

import (
	"github.com/revel/revel"
	"hapnet/app/models"
)

func (c App) LoadPage(pagecode string) revel.Result {

	data := make(map[string]interface{})
	data["success"] = true
	data["payload"] = nil

	page := new(models.Page)

	if err := Db.Where("Active=? and page_code=?", 1, pagecode).First(&page).Error; err != nil {
		data["success"] = false
		data["error"] = Error{Code: 500, Message: err.Error()}
		data["payload"] = page
		return c.RenderJSON(data)
	}

	return c.RenderTemplate(page.URL)
}
