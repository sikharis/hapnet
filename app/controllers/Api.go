package controllers

import (
	"github.com/revel/revel"
	"golang.org/x/crypto/bcrypt"
	"hapnet/app/models"
)

type Error struct {
	Code    int    ` json:"code" xml:"code" `
	Message string ` json:"message" xml:"message" `
}

type Api struct {
	GormController
}

func init() {
	revel.InterceptFunc(addHeaderCORS, revel.AFTER, &Api{})
}

func (c Api) GetProvinsi(fieldType string) revel.Result {
	// comboResult, err := c.Txn.Select(models.Provinsi{}, "SELECT id, Provinsi FROM Provinsi")
	data := make(map[string]interface{})
	provinsi := []models.Provinsi{}
	data["success"] = true
	data["payload"] = nil
	// stuff := Stuff{Foo: "xyz", Bar: 999}
	if err := Db.Find(&provinsi).Error; err != nil {
		data["success"] = false
		data["error"] = Error{Code: 500, Message: "Internal error"}
		return c.RenderJSON(data)
	}
	data["payload"] = provinsi
	return c.RenderJSON(data)
}

func (c Api) GetCityKab(provinsiId string) revel.Result {
	// comboResult, err := c.Txn.Select(models.CityKab{}, "SELECT id, CityKabType, CityKab FROM CityKab WHERE ProvinsiID = ?", provinsiId)
	data := make(map[string]interface{})
	cityKab := []models.CityKab{}
	data["success"] = true
	data["payload"] = nil
	// stuff := Stuff{Foo: "xyz", Bar: 999}
	if err := Db.Where("Provinsi_ID = ?", provinsiId).Find(&cityKab).Error; err != nil {
		data["success"] = false
		data["error"] = Error{Code: 500, Message: "Internal error"}
		return c.RenderJSON(data)
	}
	data["payload"] = cityKab
	return c.RenderJSON(data)
}

func (c Api) GetKecamatan(provinsiId string, cityKabId string) revel.Result {
	// comboResult, err := c.Txn.Select(models.Kecamatan{}, "SELECT id, Kecamatan FROM kecamatan WHERE ProvinsiID = ? AND CityKabID = ?", provinsiId, cityKabId)
	data := make(map[string]interface{})
	kecamatan := []models.Kecamatan{}
	data["success"] = true
	data["payload"] = nil
	// stuff := Stuff{Foo: "xyz", Bar: 999}
	if err := Db.Where("Provinsi_ID = ? AND City_Kab_ID = ?", provinsiId, cityKabId).Find(&kecamatan).Error; err != nil {
		data["success"] = false
		data["error"] = Error{Code: 500, Message: "Internal error"}
		return c.RenderJSON(data)
	}
	data["payload"] = kecamatan
	return c.RenderJSON(data)
}

func (c Api) GetKelurahan(provinsiId string, cityKabId string, kecamatanId string) revel.Result {
	// comboResult, err := c.Txn.Select(models.Kelurahan{}, "SELECT id, Kelurahan, PostalCode FROM Kelurahan WHERE ProvinsiID = ? AND CityKabID = ? AND KecamatanID = ?", provinsiId, cityKabId, kecamatanId)
	data := make(map[string]interface{})
	kelurahan := []models.Kelurahan{}
	data["success"] = true
	data["payload"] = nil
	// stuff := Stuff{Foo: "xyz", Bar: 999}
	if err := Db.Where("Provinsi_ID = ? AND City_Kab_ID = ? AND Kecamatan_ID = ?", provinsiId, cityKabId, kecamatanId).Find(&kelurahan).Error; err != nil {
		data["success"] = false
		data["error"] = Error{Code: 500, Message: "Internal error"}
		return c.RenderJSON(data)
	}
	data["payload"] = kelurahan
	return c.RenderJSON(data)
}

func (c Api) GetContactList(provinsiId string) revel.Result {
	// comboResult, err := c.Txn.Select(models.Contact{}, "SELECT ID, Description FROM Contact")
	data := make(map[string]interface{})
	result := []models.Contact{}
	data["success"] = true
	data["payload"] = nil
	// stuff := Stuff{Foo: "xyz", Bar: 999}
	if err := Db.Find(&result).Error; err != nil {
		data["success"] = false
		data["error"] = Error{Code: 500, Message: "Internal error"}
		return c.RenderJSON(data)
	}
	data["payload"] = result
	return c.RenderJSON(data)
}

func (c Api) GetOrderById(id string) revel.Result {
	// comboResult, err := c.Txn.Select(models.Contact{}, "SELECT ID, Description FROM Contact")
	data := make(map[string]interface{})
	result := models.Order{}
	data["success"] = true
	data["payload"] = nil

	if err := Db.Where("id=?", id).Find(&result).Error; err != nil {
		data["success"] = false
		data["error"] = Error{Code: 500, Message: err.Error()}
		return c.RenderJSON(data)
	}
	data["payload"] = result
	return c.RenderJSON(result)
}

func (c Api) UpdateStatus(from string, to string) revel.Result {
	data := make(map[string]interface{})
	provinsi := []models.StatusAccess{}
	data["success"] = true
	data["payload"] = nil

	if err := Db.Find(&provinsi).Error; err != nil {
		data["success"] = false
		data["error"] = Error{Code: 500, Message: "Internal error"}
		return c.RenderJSON(data)
	}

	data["payload"] = provinsi
	return c.RenderJSON(data)
}

func (c Api) CreateNetwork() revel.Result {
	data := make(map[string]interface{})
	data["success"] = true
	data["payload"] = nil

	var network models.Network
	c.Params.Bind(&network, "network")
	Db.Create(&network)
	if dbc := Db.Create(&network); dbc.Error != nil {
		data["success"] = false
		data["payload"] = dbc.Value
		data["error"] = Error{Code: 500, Message: dbc.Error.Error()}
		return c.RenderJSON(data)
	}

	data["payload"] = network
	return c.RenderJSON(data)
}

func (c Api) ChangePassword() revel.Result {
	data := make(map[string]interface{})
	data["success"] = true
	data["payload"] = nil

	UserId := c.Params.Form.Get("user.ID")
	OldPassword := c.Params.Form.Get("oldpassword")
	NewPassword := c.Params.Form.Get("newpassword")

	var user models.User
	bcryptPassword, _ := bcrypt.GenerateFromPassword([]byte(OldPassword), bcrypt.DefaultCost)
	if err := Db.Where("id=? and hashed_password=?", UserId, bcryptPassword).Find(&user).Error; err != nil {
		data["success"] = false
		data["error"] = Error{Code: 500, Message: "Wrong old password !"}
		return c.RenderJSON(data)
	} else {
		user.ID = uint(parseIntOrDefault(UserId, 0))
		bcryptPassword, _ = bcrypt.GenerateFromPassword([]byte(NewPassword), bcrypt.DefaultCost)
		user.HashedPassword = bcryptPassword
		Db.Save(user)
	}

	data["payload"] = nil
	return c.RenderJSON(data)
}
