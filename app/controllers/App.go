package controllers

import (
	"github.com/revel/revel"
	"golang.org/x/crypto/bcrypt"
	"hapnet/app/models"
	"hapnet/app/routes"
	// "hapnet/app/rinqueue"
)

type App struct {
	GormController
}

func init() {
	revel.InterceptFunc(addHeaderCORS, revel.AFTER, &App{})
}

// Host returns the address and port of the server, e.g. "127.0.0.1:8557"
func (c App) baseHost() string {
	return revel.Config.StringDefault("http.addr", "haha")
}

// Host returns the address and port of the server, e.g. "127.0.0.1:8557"
func (c App) Host() string {
	if revel.Server.Addr[0] == ':' {
		return "127.0.0.1" + revel.Server.Addr
	}
	return revel.Server.Addr
}

// BaseUrl returns the base http/https URL of the server, e.g. "http://127.0.0.1:8557".
// The scheme is set to https if http.ssl is set to true in the configuration file.
func (c App) BaseUrl() string {
	if revel.HTTPSsl {
		return "https://" + c.Host()
	}
	return "http://" + c.Host()
}

// simple example or user auth
func (c App) Authentication() revel.Result {
	if c.isAuthorized() {
		return c.Redirect(routes.App.Dashboard())
	} else {
		c.Flash.Error("Please log in first")
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(routes.App.Login())
	}
	return nil
}

func (c App) isAuthorized() bool {
	revel.INFO.Println("isAuthorized#username#", c.Session["username"])
	if c.Session["username"] == "" {
		return false
	}
	return true
}

func (c App) Login() revel.Result {

	return c.Render()
}

func (c App) LoginResult(user *models.User) revel.Result {
	flag := "LoginResult "
	// revel.INFO.Println(flag, "validation")
	// Username (required) must be between 4 and 15 letters (inclusive).
	c.Validation.Required(user.Username).Message("Username is required.")
	c.Validation.Required(user.Password).Message("Password is required.")

	if c.Validation.HasErrors() {
		// Store the validation errors in the flash context and redirect.
		c.Flash.Error("User not authenticated.")
		c.Validation.Keep()
		c.FlashParams()
		revel.INFO.Println(flag, "validation error: 1")
		return c.Redirect(routes.App.Login())
	}

	revel.INFO.Println(flag, "getUser")
	users := c.getUser(user.Username)
	// role := c.getRole(user.Id)

	if users != nil {
		revel.INFO.Println(flag, "CompareHashAndPassword")
		err := bcrypt.CompareHashAndPassword(users.HashedPassword, []byte(user.Password))

		if err == nil {
			revel.INFO.Println(flag, "nil condition")
			c.Session["username"] = user.Username
			if user.Remember {
				c.Session.SetDefaultExpiration()
			} else {
				c.Session.SetNoExpiration()
			}
			c.Flash.Success("Welcome, " + user.Username)
			return c.Redirect(routes.App.Dashboard())

			revel.INFO.Println(flag, err)
		}
	}

	c.Flash.Error("Wrong Username or Password.")
	c.Validation.Keep()
	c.FlashParams()
	revel.INFO.Println(flag, "validation error: 2")
	return c.Redirect(routes.App.Login())
}

func (c App) getUser(username string) *models.User {
	var user models.User
	if Db.Where("username = ?", username).First(&user).RecordNotFound() {
		c.Flash.Error("You are not logged in")
		return nil
	}
	return &user
}

// func (c App) getRole(Id int) *models.User {
// 	var user models.
// 	if Db.Where("username = ?", username).First(&user).RecordNotFound() {
// 		c.Flash.Error("You are not logged in")
// 		return nil
// 	}
// 	return &user
// }

func (c App) Logout() revel.Result {
	//RESET SESSION
	c.Session["username"] = ""
	c.Session["remember"] = "false"
	revel.INFO.Println("#username#", c.Session["username"])
	return c.Redirect("login")
}

var card1 = map[int]string{1: "S", 2: "H", 3: "C", 4: "D"}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func (c App) Testing() revel.Result {
	c.ViewArgs["foo"] = "bar"
	c.ViewArgs["bar"] = 1
	c.ViewArgs["url"] = c.baseHost()
	return c.RenderTemplate("Template/T3_P3.html")
}

// func (c App) RenderMenu() *models.Menu {
// 	menus, err := c.Txn.Select(models.Menu{}, `select * from MENU_TREE where parent_id = 0`)
// 	if err != nil {
// 		panic(err)
// 	}
// 	if len(menus) == 0 {
// 		return nil
// 	}
// 	return menus.(*models.Menu)
// }
// func (c App) Index() revel.Result {
// 	if !c.LoginCheck() {
// 		return c.Render(App.Login)
// 	}

// 	menus, err := c.Txn.Select(models.Menu{}, `select * from MENU_TREE where parent_id = 0`)
// 	if err != nil {
// 		panic(err)
// 	}
// 	// menuItems := new(models.Menu)
// 	// menuItems = c.RenderMenu()
// 	return c.Render(menus)
// }

// func (c App) Indexfull() revel.Result {
// 	return c.Render()
// }

// func (c *LoginController) Login() revel.Result {
// 	// c.Session["foo"] = "bar"
// 	// return c.Render(title, home)
// 	return c.Render()
// }

// func (c *LoginController) checkUser() revel.Result {
// 	// c.Session["foo"] = "bar"
// 	// return c.Render(title, home)
// 	return c.Render()
// }
