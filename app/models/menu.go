package models

import (
	"github.com/jinzhu/gorm"
)

type Menu struct {
	gorm.Model
	Menu_id   int
	Parent_id int
	Name      string
	Link      string
	Order_by  int
}
