package models

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/revel/revel"
	"regexp"
	"time"
)

type User struct {
	gorm.Model
	// ID             string    `json:"id"gorm:"type:char(20);primary_key"`
	Name           string    `json:"name"gorm:"type:varchar(50)"`
	Email          string    `json:"email"gorm:"type:varchar(50)"`
	Username       string    `json:"username"gorm:"type:varchar(50)"`
	Password       string    `gorm:"-"` //transient
	HashedPassword []byte    `json:"hashedpassword"`
	LastLogin      time.Time `json:"lastlogin"`
	Remember       bool      `json:"remember"`
}

// type User struct {
// 	Id                 int
// 	Name               string
// 	Username, Password string
// 	HashedPassword     []byte
// 	LastLogin          time.Time
// 	Remember           bool
// 	// Transient (this column is skipped in generated SQL statements)
// }

func (u *User) String() string {
	return fmt.Sprintf("User(%s)", u.Username)
}

var userRegex = regexp.MustCompile("^\\w*$")

func (user *User) Validate(v *revel.Validation) {
	v.Check(user.Username,
		revel.Required{},
		revel.MaxSize{15},
		revel.MinSize{4},
		revel.Match{userRegex},
	)

	ValidatePassword(v, user.Password).
		Key("user.Password")

	v.Check(user.Name,
		revel.Required{},
		revel.MaxSize{100},
	)
}

func ValidatePassword(v *revel.Validation, password string) *revel.ValidationResult {
	return v.Check(password,
		revel.Required{},
		revel.MaxSize{15},
		revel.MinSize{5},
	)
}
