package models

type Provinsi struct {
	Id       string `json:"id"gorm:"type:int(11);AUTO_INCREMENT"`
	Provinsi string `json:"provinsi"gorm:"type:varchar(50)"`
}

type CityKab struct {
	Id          string `json:"id"gorm:"type:int(11);AUTO_INCREMENT"`
	ProvinsiId  string `json:"provinsi_id"gorm:"type:int(11)"`
	CityKabType string `json:"city_kab_type"gorm:"type:varchar(5)"`
	CityKab     string `json:"city_kab"gorm:"type:varchar(50)"`
}

type Kecamatan struct {
	Id        string `json:"id"gorm:"type:int(11);AUTO_INCREMENT"`
	Kecamatan string `json:"kecamatan"gorm:"type:varchar(50)"`
}

type Kelurahan struct {
	Id         string `json:"id"gorm:"type:int(11);AUTO_INCREMENT"`
	Kelurahan  string `json:"kelurahan"gorm:"type:varchar(50)"`
	PostalCode string `json:"postal_code"gorm:"type:char(5)"`
}
