package models

import (
	"github.com/jinzhu/gorm"
)

type Order struct {
	gorm.Model
	OrderID       string `json:"orderid"gorm:"type:char(20);unique_index"`
	ProductCode   string `json:"productcode"`
	Phone         string `json:"phone"`
	Status        string `json:"status"`
	Name          string `json:"name"`
	Address       string `json:"address"`
	NumberOfOrder int    `json:"numberoforder"`
	PacketType    string `json:"packettype"`
	Email         string `json:"email"`
	//Payment
	PaymentMethod string `json:"paymentmethod"`
	BankCode      string `json:"bankcode"`
	PaymentAmount string `json:"paymentamount"`
	//Address
	ProvinsiID  string `json:"provinsiid"`
	CityKabID   string `json:"citykabid"`
	KecamatanID string `json:"kecamatanid"`
	KelurahanID string `json:"kelurahanid"`
}
