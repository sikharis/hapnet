package models

import (
	"github.com/jinzhu/gorm"
)

type Status struct {
	gorm.Model
	StatusCode  string `json:"role"gorm:"type:varchar(10);unique_index"`
	Name        string `json:"role"gorm:"type:varchar(25);unique_index"`
	Description string `json:"description"gorm:"type:varchar(30)"`
}

type StatusAccess struct {
	gorm.Model
	StatusId     int  `json:"statusid"gorm:"unique_index:idx_statusaccess_unique"`
	NextStatusId int  `json:"nextstatusid"gorm:"unique_index:idx_statusaccess_unique"`
	Order        int  `json:"order"`
	Active       bool `json:"active"gorm:"type:tinyint(1)"`
}
