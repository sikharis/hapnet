package models

import (
	"github.com/jinzhu/gorm"
)

type Contact struct {
	gorm.Model
	Description string `json:"description"gorm:"type:varchar(50)"`
}
