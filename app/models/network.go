// models.network.go

package models

import (
	"github.com/jinzhu/gorm"
)

type Network struct {
	gorm.Model
	NetworkName   string `gorm:"type:varchar(50)"`
	Description   string `gorm:"type:varchar(200)"`
	PostbackURL   string `gorm:"type:varchar(100)"`
	PostbackLeads string `gorm:"type:varchar(100)"`
	PostbackSales string `gorm:"type:varchar(100)"`
	ClickIDSale   string `gorm:"type:varchar(100)"`
}
