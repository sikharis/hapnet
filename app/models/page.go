package models

import (
	"github.com/jinzhu/gorm"
)

type Page struct {
	gorm.Model
	Name        string `json:"code"gorm:"type:varchar(50)"`
	PageCode    string `json:"pagecode"gorm:"type:varchar(20)"`
	URL         string `json:"url"gorm:"type:varchar(100)"`
	Description string `json:"description"gorm:"type:varchar(100)"`
	Active      bool   `json:"active"gorm:"type:tinyint(1)"`
}

type PageAccess struct {
	gorm.Model
	UserId int  `json:"userid"`
	PageId int  `json:"pageid"`
	Active bool `json:"active"gorm:"type:tinyint(1)"`
}
