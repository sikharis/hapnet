package models

import (
	"github.com/jinzhu/gorm"
)

type Product struct {
	gorm.Model
	ProductCode string `json:"productcode"gorm:"type:varchar(10);unique_index"`
	Name        string `json:"name"gorm:"type:varchar(20)"`
	Description string `json:"productcode"gorm:"type:varchar(100)"`
	Domain      string `json:"url"gorm:"type:varchar(50)"`
	Active      bool   `json:"active"gorm:"type:tinyint(1)"`
}

type ProductTemplate struct {
	gorm.Model
	ProductId   int    `json:"productid"gorm:"type:int(11)"`
	NextId      int    `json:"nextcode"gorm:"type:int(11)"`
	Name        string `json:"code"gorm:"type:varchar(50)"`
	URL         string `json:"url"gorm:"type:varchar(100)"`
	Description string `json:"description"gorm:"type:varchar(100)"`
	Active      bool   `json:"active"gorm:"type:tinyint(1)"`
}

type ProductStats struct {
	gorm.Model
	ProductId   int  `json:"productid"gorm:"type:int(11)"`
	TemplateId  int  `json:"templateid"`
	Count       int  `json:"count"gorm:"type:int(11)"`
	LandingPage bool `json:"landingpage"gorm:"type:tinyint(1)"`
}
