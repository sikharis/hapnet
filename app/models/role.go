package models

import (
	"github.com/jinzhu/gorm"
)

type Role struct {
	gorm.Model
	Role        string `json:"role"gorm:"type:varchar(10);unique_index"`
	Description string `json:"description"gorm:"type:varchar(30)"`
}

type RoleAccess struct {
	gorm.Model
	UserId int  `json:"userid"`
	RoleId int  `json:"roleid"`
	Active bool `json:"active"gorm:"type:tinyint(1)"`
}
