package models

import (
	"time"
)

type Traffic struct {
	GUID       string `json:"guid"gorm:"type:char(20);primary_key"`
	GUIDTime   time.Time
	URL        string `json:"url"gorm:"type:varchar(100)"`
	Page       int    `json:"page"`
	IP         string `json:"ip"gorm:"type:varchar(20)"`
	NetworkID  int    `json:"networkid"gorm:"type:int(11)"`
	ClickID    int    `json:"clickid"gorm:"type:int(11)"`
	TID        int    `json:"tid"gorm:"type:int(11)"`
	GCLID      int    `json:"gclid"gorm:"type:int(11)"`
	UserAgent  string `json:"useragent"gorm:"type:varchar(255)"`
	DeviceType string `json:"devicetype"gorm:"type:varchar(6)"`
	P1         string `json:"p1"gorm:"type:varchar(100)"`
	P2         string `json:"p2"gorm:"type:varchar(100)"`
	P3         string `json:"p3"gorm:"type:varchar(100)"`
	P4         string `json:"p4"gorm:"type:varchar(100)"`
	P5         string `json:"p5"gorm:"type:varchar(100)"`
}

// func (traffic *Traffic) Validate(v *revel.Validation) {

// 	v.Check(traffic.GUID, revel.Required{}, revel.MaxSize{20}, revel.MinSize{20})
// 	v.Check(traffic.URL, revel.Required{}, revel.MaxSize{200}, revel.MinSize{0})
// 	v.Check(traffic.NetworkID, revel.Required{}, revel.MaxSize{5}, revel.MinSize{1})
// 	v.Check(traffic.ClickID, revel.Required{}, revel.MaxSize{200}, revel.MinSize{1})
// 	v.Check(traffic.TID, revel.Required{}, revel.MaxSize{200}, revel.MinSize{1})
// }
